#for pure wayland with -X make dummy symbols
#load using  env LD_PRELOAD=~/shim.so
#include <stdlib.h>
void gdk_x11_display_get_xdisplay() { abort(); }
void gdk_x11_get_default_root_xwindow() { abort(); }
void gdk_x11_get_default_xdisplay() { abort(); }
void cairo_xlib_surface_create() { abort(); }
void gdk_x11_xatom_to_atom() { abort(); }
void gdk_x11_window_get_xid() { abort(); }
void gdk_x11_get_xatom_by_name_for_display()  { abort(); }
void gdk_x11_display_get_user_time() { abort(); }
void gdk_x11_screen_supports_net_wm_hint() { abort(); }
void gdk_x11_screen_get_screen_number() { abort(); }
void gdk_x11_screen_lookup_visual() { abort(); }
void gdk_x11_get_xatom_by_name() { abort(); }
void gdk_x11_window_lookup_for_display() { abort(); }
void gdk_x11_get_server_time() { abort(); }
void gdk_x11_atom_to_xatom() { abort(); }
void gdk_x11_lookup_xdisplay() { abort(); }
void gdk_x11_set_sm_client_id() { abort(); }